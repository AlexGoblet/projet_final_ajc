# Projet Final

## 1° Infrastructure

L'image ci-dessous montre l'infrastructure de notre projet. La solution Cloud retenue : Amazon web services. Le cloud AWS ne servira que de ressources VM.

**![](https://lh4.googleusercontent.com/XxZ0uqhT7U74kZpNkltTEpP1WVBO-X68LPF3v594ejqZ9hFrbY8-1itzgx9IxXLjVD3dxDCEp9rooadq0vso_nmg9_OI8tdT6RcElhaw1qPOnMYUEEmTx5PoIOolGfiJSuBGZVu7)**

Pour le provisinning nous allons utiliser Terraform. Création de 5 VM AWS et nous utiliserons 3 autres VM (1 chacun qui nous permettra de faire le rôle d'administrateur et de dévellopeur)

## 2° Terraform

Le code Terraform est dans le Gitlab. Il faut un compte AWS, l'access key et une secret key pour pouvoir l'utiliser.

Les 5 VM sont bien crées, notre Terraform permet de récupérer l'adresse IP public et les clefs privées. Le fichier hosts est crée dynamiquement. Cela nous sera utile pour lancer des playbooks avec Ansible. 

### Site 1 : Jekyll

Pour ce site, nous allons utiliser 4 VMs différentes (Alexandre, Vincent, VM1 et VM3). 

Dans les VMs d'Alexandre et Vincent nous avons Ansible pour lancer les playbooks. Les playbooks vont nous permettre d'installer des packages sur les VM : 

    VM1 : Docker, Gitlab, Gitlab-runner,

    VM3: Apache2

- Mode Administrateur

A la suite de l'installation des packages, nous enregistrons un runner pour activer la pipeline avec Gitlab. Le runner se lancera a chaque commit grâce au fichier gitlab-ci.yml

!!!!! Le dossier /var/www/html étant protégé ne pas oublier de lui donner les droits avec la commande chmod. !!!!!!!

- Mode Devellopeur

Sur les machines des dévelopeurs, installation de git qui va permettre de faire des push. Le push va déclencher un pipeline. Conversion du fichier.md en fichier.html (grâce à Jekyll). Le fichier HTML va être envoyé sur la machine VM3 qui stocke le site, dans le dossier /var/www/html/.

*Les machines d'Alexandre et Vincent sont d'abord en mode Administrateur (Terraform, Ansible, Droits) avant de passer en mode dévelopeur pour les tests (commit sur Gitlab).*

### Site 2 : LAMP

**LAMP** est un acronyme pour Linux, Apache, MySQL, PHP. Nous utilisons le même principe que pour le site Jekyll. La VM2 servira pour le pipeline pendant que les VMs d'Alexandre et Vincent seront toujours administrateur et dévelopeur. 

Pour les images et les fichiers.php, nous utilisons le GitHub de  [GitHub - qyjohn/simple-lamp: Simple LAMP Web Application](https://github.com/qyjohn/simple-lamp)

VM1 : Docker, Gitlab, Gitlab-runner

VM4 : Apache2,Mariadb, PHP. (rôle lamp)

- Mode Administrateur

Nous devions créer un User et une database pour Mariadb. Nous l'avons réalisé à la main et pas par Ansible. Les administrateurs sont donc aller sur la VM4 pour se connecter à mariadb et faire les créations nécessaires  (Automatisation à prévoir)

```
$ mysql -u root -p
mysql> CREATE DATABASE simple_lamp;
mysql> CREATE USER 'username'@'localhost' IDENTIFIED BY 'password';
mysql> GRANT ALL PRIVILEGES ON simple_lamp.* TO 'username'@'localhost';
mysql> quit
$ cd /var/www/html/simple-lamp
$ mysql -u username -p simple_lamp < simple_lamp.sql
```

Avant que nous puissions le faire fonctionner, quelques modifications mineures sont nécessaires :

(1) Utilisez un éditeur de texte pour ouvrir config.php, puis modifiez le nom d'utilisateur et le mot de passe de votre installation MySQL.

(2) Changez la propriété du dossier "uploads" en "www-data" afin qu'Apache puisse télécharger des fichiers dans ce dossier.

```
$cd /var/www/html/simple-lamp
$ sudo chown -R www-data:www-data uploads
```

- Mode Developpeur

Le développeur modifie donc le fichier.php et le push sur le gitlab. La pipeline va se mettre en place grâce au runner créé précedemment. Le .gitlab-ci.yml, va permettre d'envoyer les modifications sur la VM4. 

### Site 3 : WEB Python

Utilisation de la VM2 et VM5 créé avec notre Terraform.

    VM2 : Docker, Jenkins,

    VM5 : Docker

Pour le site 3 le but était de pouvoir redéployer une application web python tournant au sein d'un conteneur sur une VM dès lors qu'un push serait executé par un développeur sur le projet git de l'application.
L'application web en question est une simple page index.html affichée par une application Flask. Lorsque l'appli tourne, on y accède dans le navigateur en indiquant l'IP de la machine (sur le port 5000) qui fait tourner le conteneur avec l'appli. 
L'appli est hébergée dans un repo sur la machine Gitlab. Le repo contenant :

- myapp.py : fichier principal qui va être run pour lancer l'app
- index.html : la page (contenue dans un dossier templates) qui sera affichée sur http://18.197.51.251:5000/index
- Dockerfile : pour construire l'image docker qui va contenir l'application (installe flask et pytest-flask)
- test_.py : pour la phase de test sur l'appli

Pour réaliser cette tâche, le terraform provisionne deux machines: l'une avec Docker et Jenkins d'installés dessus et l'autre avec uniquement Docker. 
Une fois Jenkins configuré on y crée deux jobs:

*test_python_app*: Ce job est déclenché au moindre push sur le repo git de l'appli. Il utilise alors le Dockerfile du repo pour construire une image docker qui va contenir l'appli dans sa dernière version, puis un conteneur qui va faire tourner le tout. La commande lancée au moment du docker run teste avec pytest l'accès à la page index.html : 

```
docker run -d --name $CONTAINER_NAME $IMAGE_NAME pytest -v --junitxml=reports/result.xml
```

Si le test réussit, un deuxième job est déclenché pour déployer l'application. 

*deploy_python_app*: Ce job est déclenché lorsque *test_python_app* réussit. De la même manière que test_python_app il construit l'image en y mettant l'appli dans sa dernière version. Il envoie ensuite l'image par ssh sur la machine cible. Puis on lance le conteneur sur la machine cible en ssh en utilisant la commande:

```
ssh -i $PRIV admin@$HOST docker run -p 5000:5000 -d --name $CONTAINER_NAME $IMAGE_NAME python3 myapp.py
```

Enfin, pour mettre en place le lien entre le job *test_python_app* et Gitlab on paramètre le job en question de manière à ce qu'il utilise le lien du repo dans la gestion de code source et on récupère l'URL qui nous est fournit par Jenkins comme hook pour ensuite mettre en place un webhook avec cette URL sur le repo de l'application. Une fois tout cela fait, enjoy !

## Optimisation Future

1. Mettre de la haute disponibilté

2. Faire des tests avant le déployement des sites 1 et 2
   
   Php unit

3. Utiliser Gitlab avec Docker
   
   Pour la VM2, nous avons choisi d'installer Gitlab sans passer par docker. Si nous avions plus de temps, nous recommencerons cette VM en installant Gitlab avec Docker. Cela nous evitera d'avoir un stockage insuffisant dans la VM et de devoir rajouter du Stockage en court de projet. 
