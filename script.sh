#!/bin/bash

### PROVISIONING TERRAFORM ###

PRIVATE_KEY=private_key.pem
HOSTS=inventory
machines=( "lamp" "jekyll" "python" "jenkins" "gitlab" )


#on lance le terraform

terraform init && terraform plan && terraform apply


#on recupère la clé privée output par terraform

terraform output -raw private_key > $PRIVATE_KEY
chmod 400 $PRIVATE_KEY


#si le fichier hosts personnalisé existe dejà on le supprime

if [ -f $HOSTS ]; then
	rm $HOSTS
fi


#on crée le fichier hosts personnalisé avec les ips output par terraform

for machine in ${machines[@]}; do
	echo "[$machine]" >> $HOSTS
	echo "$(terraform output -raw public_ip_$machine) ansible_ssh_private_key_file=\"$PRIVATE_KEY\"" >> $HOSTS
done

### iNSTALLATION WITH ANSIBLE ###


for machine in ${machines[@]}; do
	ansible-playbook -i $HOSTS playbook $machine.yml
done
