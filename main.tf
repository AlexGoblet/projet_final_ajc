provider "aws"{
    region = var.AWS_REGION
    access_key = var.AWS_ACCESS_KEY
    secret_key = var.AWS_SECRET_KEY
}

resource "tls_private_key" "example" {
    algorithm = "RSA"
    rsa_bits  = 4096
}

resource "aws_key_pair" "my_key_pair" {
    key_name   = "projet-mav-key"
    public_key = tls_private_key.example.public_key_openssh
}

resource "aws_security_group" "my_sg" {
    name = "projet-mav-sg"
    egress {
     from_port = 0
     to_port = 0
     protocol = "-1"
     cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
     from_port = 80
     to_port = 80
     protocol = "tcp"
     cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
     from_port = 22
     to_port = 22
     protocol = "tcp"
     cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
     from_port = 443
     to_port = 443
     protocol = "tcp"
     cidr_blocks = ["0.0.0.0/0"]
    }    
    ingress {
     from_port = 8080
     to_port = 8080
     protocol = "tcp"
     cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
     from_port = 5000
     to_port = 5000
     protocol = "tcp"
     cidr_blocks = ["0.0.0.0/0"]
    }

}

resource "aws_instance" "my_lamp" {
    ami = var.AWS_AMI
    instance_type = "t2.micro"
    key_name = aws_key_pair.my_key_pair.key_name
    vpc_security_group_ids = [aws_security_group.my_sg.id]
    user_data = <<EOF
    #!/bin/bash
    sudo apt update -y
    sudo apt install apache2 php libapache2-mod-php mariadb-server php-mysql
    EOF
    tags = {
        Name = "projet-mav-lamp"
    }
}

resource "aws_instance" "my_jekyll" {
    ami = var.AWS_AMI
    instance_type = "t2.micro"
    key_name = aws_key_pair.my_key_pair.key_name
    vpc_security_group_ids = [aws_security_group.my_sg.id]
    tags = {
        Name = "projet-mav-jekyll"
    }
}

resource "aws_instance" "my_python" {
    ami = var.AWS_AMI
    instance_type = "t2.micro"
    key_name = aws_key_pair.my_key_pair.key_name
    vpc_security_group_ids = [aws_security_group.my_sg.id]
    tags = {
        Name = "projet-mav-python"
    }
}

resource "aws_instance" "my_jenkins" {
    ami = var.AWS_AMI
    instance_type = "t2.medium"
    key_name = aws_key_pair.my_key_pair.key_name
    vpc_security_group_ids = [aws_security_group.my_sg.id]
    tags = {
        Name = "projet-mav-jenkins"
    }
}

resource "aws_instance" "my_gitlab" {
    ami = var.AWS_AMI
    instance_type = "t2.large"
    key_name = aws_key_pair.my_key_pair.key_name
    vpc_security_group_ids = [aws_security_group.my_sg.id]
    tags = {
        Name = "projet-mav-gitlab"
    }
}

output "private_key" {
  value     = tls_private_key.example.private_key_pem
  sensitive = true
}

output "public_ip_lamp" {
    description = "Public IP address of the lamp instance"
    value       = aws_instance.my_lamp.public_ip
}

output "public_ip_jekyll" {
    description = "Public IP address of the jekyll instance"
    value       = aws_instance.my_jekyll.public_ip
}

output "public_ip_python" {
    description = "Public IP address of the python instance"
    value       = aws_instance.my_python.public_ip
}

output "public_ip_jenkins" {
    description = "Public IP address of the Jenkins instance"
    value       = aws_instance.my_jenkins.public_ip
}

output "public_ip_gitlab" {
    description = "Public IP address of the Gitlab instance"
    value       = aws_instance.my_gitlab.public_ip
}